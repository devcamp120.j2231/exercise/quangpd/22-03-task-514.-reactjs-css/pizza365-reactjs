//Import thư viện boostrap css
import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';

import Header from "./components/header/header";
import Content from "./components/content/content";
import Footer from "./components/footer/footer";


function App() {
  return (
    <div className="container-fluid">
      
      <Header/>

      
      <Content/>

     
      <Footer/>

    </div>
  );
}

export default App;
