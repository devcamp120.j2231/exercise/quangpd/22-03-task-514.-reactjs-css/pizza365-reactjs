import { Component } from "react";

class Footer extends Component{
    render (){
        return (
            <>
                <div>
                    <div className="container-fluid  p-5" style={{backgroundColor :"orange"}}>
                        <div className="row text-center">
                            <div className="col-sm-12">
                                <h4 className="m-2">Footer</h4>
                                <a href="#" className="btn btn-dark m-3"><i className="fa fa-arrow-up"></i>&nbsp;To the top</a>
                                <div className="m-2">
                                    <i className="fab fa-facebook-square"></i>
                                    <i className="fab fa-instagram"></i>
                                    <i className="fab fa-snapchat"></i>
                                    <i className="fab fa-pinterest-p"></i>
                                    <i className="fab fa-twitter"></i>
                                    <i className="fab fa-linkedin-in"></i>
                                </div>
                                <div className="m-2">
                                    <strong className="text-center">Powered by DEVCAMP</strong>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </>
        )
    }
}

export default Footer