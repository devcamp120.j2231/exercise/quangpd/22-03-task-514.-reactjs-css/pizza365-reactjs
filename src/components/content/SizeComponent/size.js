import { Component } from "react";
//Import thư viện boostrap css
import "bootstrap/dist/css/bootstrap.min.css";

class Size extends Component{
    render(){
        return (
            <>
                <div>
                    <div id="plans" className="row">
                        {/* Title Chọn Size Pizza */}
                        <div className="col-sm-12 text-center p-4 mt-4 text-warning">
                            <h2><b className="p-1 border-bottom border-warning">Chọn size pizza</b></h2>
                            <p style={{fontWeight : "500"}}>Chọn combo pizza phù hợp với nhu cầu của bạn</p>
                        </div>

                        {/* Content Chọn Size Pizza Detail */}
                        <div className="col-sm-12">
                            <div className="row">
                                <div className="col-sm-4">
                                    <div className="card">
                                        <div className="card-header text-center" style={{backgroundColor : "orange"}} > 
                                            <h3>S (small)</h3>
                                        </div>
                                        <div className="card-body text-center">
                                            <ul className="list-group list-group-flush">
                                            <li className="list-group-item">Đường kính : <b>20cm</b></li>
                                            <li className="list-group-item">Sườn nướng : <b>2</b></li>
                                            <li className="list-group-item">Salad : <b>200g</b></li>
                                            <li className="list-group-item">Nước ngọt : <b>2</b></li>
                                            <li className="list-group-item"><h1>150.000</h1>VNĐ</li>
                                            </ul>
                                        </div>
                                    <div className="card-footer text-center">
                                        <button id="btn-order-size-s" style={{backgroundColor : "orange"}} className="btn w-100"  data-is-selected-size = "N"><b>Chọn</b></button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="card">
                                    <div style={{maxHeight: "70px"}} className="card-header bg-warning  text-center">
                                        <h3>M (medium)</h3>
                                    </div>
                                    <div className="card-body text-center">
                                        <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Đường kính : <b>25cm</b></li>
                                        <li className="list-group-item">Sườn nướng : <b>4</b></li>
                                        <li className="list-group-item">Salad : <b>300g</b></li>
                                        <li className="list-group-item">Nước ngọt : <b>3</b></li>
                                        <li className="list-group-item"><h1>200.000</h1>VNĐ</li>
                                        </ul>
                                    </div>
                                <div className="card-footer text-center">
                                    <button id="btn-order-size-m" style={{backgroundColor : "orange"}} className="btn w-100"  data-is-selected-size = "N"><b>Chọn</b></button>
                                </div>
                            </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="card">
                                    <div className="card-header text-center" style={{backgroundColor : "orange"}}>
                                        <h3>L (large)</h3>
                                    </div>
                                    <div className="card-body text-center">
                                        <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Đường kính : <b>30cm</b></li>
                                        <li className="list-group-item">Sườn nướng : <b>8</b></li>
                                        <li className="list-group-item">Salad : <b>500g</b></li>
                                        <li className="list-group-item">Nước ngọt : <b>4</b></li>
                                        <li className="list-group-item"><h1>250.000</h1>VNĐ</li>
                                        </ul>
                                    </div>
                                <div className="card-footer text-center">
                                    <button id="btn-order-size-l" style={{backgroundColor : "orange"}} className="btn w-100"  data-is-selected-size = "N"><b>Chọn</b></button>
                                </div>
                            </div>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>            
            </>
        )
    }
}

export default Size