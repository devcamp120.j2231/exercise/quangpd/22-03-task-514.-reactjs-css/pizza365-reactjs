import { Component } from "react";

class Drink extends Component{
    render(){
        return(
            <>
                <div>
                    <div className="container">
                        <div className="row form-group">
                            <div className="col-sm-12 text-center p-4 mt-4">
                                <h2><b className="p-2 border-bottom border-warning text-warning">Chọn đồ uống</b></h2>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                <select className="form-control"  id="select-drink">
                                    <option value="0">Tất cả các loại nước uống</option>
                                    <option value="TRATAC">Trà tắc</option>
                                    <option value="COCA">Cocacola</option>
                                    <option value="PEPSI">Pepsi</option>
                                    <option value="LAVIE">Lavie</option>
                                    <option value="TRASUA">Trà sữa trân châu</option>
                                    <option value="FANTA">Fanta</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
export default Drink