import { Component } from "react";
//Import thư viện boostrap css
import "bootstrap/dist/css/bootstrap.min.css";
import Size from "./SizeComponent/size";
import Introduce from "./IntroduceComponent/introduce";
import Type from "./TypeComponent/type";
import Drink from "./DrinkComponent/drink";
import Form from "./FormComponent/form";

class Content extends Component{
    render(){
        return(
            <>
                <div>
                    <div className="container" style={{padding : "120px 0 50px 0" }}>
                        <div className="row">
                            <div className="col-sm-12">
                                {/* introduce */}
                                <Introduce/>

                                {/* size */}

                                <Size/>

                                {/* Type */}
                                <Type/>
                                
                                {/* Drink */}
                                <Drink/>

                                {/* Form */}
                                <Form/>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Content