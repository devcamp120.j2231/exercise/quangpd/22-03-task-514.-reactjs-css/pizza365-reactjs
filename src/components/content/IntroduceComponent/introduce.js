import { Component } from "react";
import image1 from "../../../assets/images/1.jpg";
import image2 from "../../../assets/images/2.jpg";
import image3 from "../../../assets/images/3.jpg";
import image4 from "../../../assets/images/4.jpg";
class Introduce extends Component{
    render(){
        return(
            <>
                <div>
                    <div className="row">
                                        {/* Title row home */}
                                        <div className="col-sm-12 text-warning">
                                            <h1><b>PIZZA 365</b></h1> 
                                            <p style={{fontStyle : "italic"}}>Truly italian ! </p>
                                        </div>

                                        {/* Slide row home */}
                                        <div className="col-sm-12">
                                            <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                                            <ol className="carousel-indicators">
                                                <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                                            </ol>
                                            <div className="carousel-inner">
                                                <div className="carousel-item active">
                                                <img className="d-block w-100" src = {image1} alt="First slide"/>
                                                </div>
                                                <div className="carousel-item">
                                                <img className="d-block w-100" src= {image2} alt="Second slide"/>
                                                </div>
                                                <div className="carousel-item">
                                                <img className="d-block w-100" src= {image3} alt="Third slide"/>
                                                </div>
                                                <div className="carousel-item">
                                                <img className="d-block w-100" src= {image4} alt="Fourth slide"/>
                                                </div>
                                            </div>
                                            <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span className="sr-only">Previous</span>
                                            </a>
                                            <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span className="sr-only">Next</span>
                                            </a>
                                            </div>
                                        </div>

                                        {/* Title Tại Sao Lại Pizza365 */}
                                        <div className="col-sm-12 text-center p-4 mt-4 text-warning">
                                            <h2><b className="p-2 border-bottom border-warning">Tại Sao Lại Pizza 365</b></h2>
                                        </div>

                                        {/* Content Tại Sao Lại Pizza 365 */}
                                        <div className="col-sm-12 border border-warning">
                                            <div className="row">
                                            <div className="col-sm-3 p-4  border" style={{backgroundColor : "lightgoldenrodyellow"}}>
                                                <h3 className="p-2">Đa Dạng</h3>
                                                <p className="p-2">Số lượng pizza đa dạng,có đầy đủ các loại pizza đang hot nhất hiện nay.</p>
                                            </div>
                                            <div className="col-sm-3 p-4   border " style={{backgroundColor : "yellow"}}>
                                                <h3 className="p-2">Chất lượng</h3>
                                                <p className="p-2">Nguyên liệu sạch 100% rõ nguồn gốc,quy trình chế biến đẩm bảo vệ sinh an toàn thực phẩm</p>
                                            </div>
                                            <div className="col-sm-3 p-4   border" style={{backgroundColor : "lightsalmon"}}>
                                                <h3 className="p-2">Hương vị</h3>
                                                <p className="p-2">Đảm bảo hương vị ngon,độc lạ mà bạn chỉ có thể trải nghiệm từ Pizza 365</p>
                                            </div>
                                            <div className="col-sm-3 p-4   border" style={{backgroundColor : "orange"}}>
                                                <h3 className="p-2">Dịch vụ</h3>
                                                <p className="p-2 ">Nhân viên thân thiện,nhà hàng hiện đại. Dịch vụ giao hàng nhanh chất lượng,tân tiến. </p>
                                            </div>
                                            </div>
                                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Introduce