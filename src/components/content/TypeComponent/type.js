import { Component } from "react";
//Import thư viện boostrap css
import "bootstrap/dist/css/bootstrap.min.css";
import pizzahaisan from "../../../assets/images/seafood.jpg";
import pizzahawai from "../../../assets/images/hawaiian.jpg";
import pizzabacon from "../../../assets/images/bacon.jpg";
class Type extends Component{
    render() {
        return(
            <>
                <div>
                    <div id="about" className="row">
                        {/* Title Chọn Loại Pizza */}
                        <div className="col-sm-12 text-center p-4 mt-4">
                            <h2><b className="p-2 border-bottom border-warning text-warning">Chọn Loại pizza</b></h2>
                        </div>

                        {/* Content Chọn Loại Pizza */}
                        <div className="col-sm-12">
                            <div className="row">
                                <div className="col-sm-4">
                                    <div className="card w-100  " style={{width : "18rem"}}>
                                        <img src={pizzahaisan} className="card-img-top"/>
                                    <div className="card-body card-body-pizza-type">
                                        <h5>OCEAN MANIA</h5>
                                        <p>PIZZA HẢI SẢN XỐT MAYONNAISE</p>
                                        <p>Xốt Cà Chua, Phô Mai Mozzarella , Tôm , Mực , Thanh Cua , Hành Tây.</p>
                                    </div>
                                    <div className="card-footer">
                                        <p><button className="btn  w-100" id="btn-pizza-ocean" style={{backgroundColor : "orange"}}  data-is-selected-pizza="N"><b>Chọn</b></button></p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="card w-100 " style={{width : "18rem"}}>
                                    <img src={pizzahawai} className="card-img-top"/>
                                <div className="card-body card-body-pizza-type">
                                    <h5>HAWAIIAN</h5>
                                    <p>PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
                                    <p>Xốt Cà Chua , Phô Mai Mozzarella , Thịt Dăm Bông ,Thơm.</p>
                                </div>
                                <div className="card-footer">
                                    <p><button className="btn  w-100" id="btn-pizza-hawai" style={{backgroundColor : "orange"}} data-is-selected-pizza="N"><b>Chọn</b></button></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-4">
                            <div className="card w-100 " style={{width : "18rem"}}>
                                <img src={pizzabacon}  className="card-img-top"/>
                            <div className="card-body card-body-pizza-type">
                                <h5>PHÔ MAI CHICKEN BACON</h5>
                                <p> PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI </p>
                                <p>Xốt Phô Mai , Thịt Gà , Thịt Heo Muối, Phô Mai Mozzarella , Cà Chua.</p>
                            </div>
                            <div className="card-footer">
                                <p><button className="btn  w-100" id="btn-pizza-bacon" style={{backgroundColor : "orange"}} data-is-selected-pizza="N"><b>Chọn</b> </button></p>
                            </div>
                        </div>
                    </div>
                            </div>
                        </div>
                        </div>
                </div>
            </>
        )
    }
}

export default Type