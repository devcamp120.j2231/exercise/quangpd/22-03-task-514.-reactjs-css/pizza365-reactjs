import { Component } from "react";

class Form extends Component{
    render(){
        return (
            <>
                <div>
                    <div id="contact" className="row">
                        {/* title Gửi Đơn Hàng */}
                        <div className="col-sm-12 text-center p-4 mt-4">
                            <h2><b className="p-2 border-bottom border-warning text-warning">Gửi Đơn Hàng</b></h2>
                        </div>

                        {/* Content Gửi Đơn Hàng */}
                        <div className="col-sm-12 p-2 ">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="form-group">
                                        <label>Tên</label>
                                        <input type="text" className="form-control" id="inp-fullname" placeholder="Họ và tên"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Email</label>
                                        <input type="text" className="form-control" id="inp-email" placeholder="Email"/>
                                    </div>
                                    <div className="form-group">
                                        <label> Số Điện thoại</label>
                                        <input type="text" className="form-control" id="inp-dien-thoai" placeholder="Điện thoại"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Địa chỉ</label>
                                        <input type="text" className="form-control" id="inp-dia-chi" placeholder="Địa chỉ"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Mã Giảm Giá </label>
                                        <input type="text" className="form-control" id="inp-ma-giam-gia" placeholder="Mã Giảm Giá"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Lời nhắn</label>
                                        <input type="text" className="form-control" id="inp-message" placeholder="Lời nhắn"/>
                                    </div>
                                    <button type="button" id="btn-send" style={{backgroundColor : "orange"}}  className="btn w-100"> <b>Gửi</b> </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Form